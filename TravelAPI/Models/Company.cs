﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TravelAPI
{
    public class Company
    {
        public Company() { }

        public Company(string Name)
        {
            this.Name = Name;
        }

        public string Name { get; set; }
    }
}