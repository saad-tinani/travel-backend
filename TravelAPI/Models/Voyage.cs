﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TravelAPI.Models;

namespace TravelAPI
{
    public class Voyage
    {
        public int Id { get; set; }
        public int DepartureLocationId { get; set; }
        public City Depart { get; set; }
        public int ArrivalLocationId { get; set; }
        public City Arrival { get; set; }
        public string DepartTime { get; set; }
        public string ArrivalTime { get; set; }
        // public DateTime DepartTime { get; set; }
        // public DateTime ArrivalTime { get; set; }
        public Double Price { get; set; }
        public string Stations { get; set; }
        public string Type { get; set; }
        public Company Company { get; set; }
    }
}