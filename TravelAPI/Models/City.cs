﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace TravelAPI.Models
{
    [Table("Cities")]
    public class City
    {
        public City() { }

        public City(string Code, string Name)
        {
            this.Code = Code;
            this.Name = Name;
        }

        [Key]
        public int Id { get; set; }
        public string Code { get; set; }
        [Required]
        public string Name { get; set; }
        public double? Lat { get; set; }
        public double? Lng { get; set; }
    }
}