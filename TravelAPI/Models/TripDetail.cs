﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TravelAPI.Models
{
    [Table("TripsDetails")]
    public class TripDetail
    {
        public int Id { get; set; }
        [Required]
        public int TripId { get; set; }
        public Trip Trip { get; set; }
        [Required]
        public int DepartureLocationId { get; set; }
        public City DepartureLocation { get; set; }
        [Required]
        public int ArrivalLocationId { get; set; }
        public City ArrivalLocation { get; set; }
        [Required]
        public string DepartureTime { get; set; }
        [Required]
        public string ArrivalTime { get; set; }
        [Required]
        public float Price { get; set; }
    }
}