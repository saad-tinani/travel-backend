﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TravelAPI.Models;

namespace TravelAPI
{
    interface ICityDal : IDisposable
    {
        void AddCity(City city);
        City GetCity(int id);
        List<City> GetAllCities();
    }
}
