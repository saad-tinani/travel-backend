﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TravelAPI.Models;

namespace TravelAPI
{
    public class CityDal : ICityDal
    {
        private BddContext bdd;

        public CityDal()
        {
            bdd = new BddContext();
        }

        public void Dispose()
        {
            bdd.Dispose();
        }

        public void AddCity(City city)
        {
            bdd.Cities.Add(city);
            bdd.SaveChanges();
        }

        public City GetCity(int id)
        {
            return bdd.Cities.Find(id);
        }

        public List<City> GetAllCities()
        {
            return bdd.Cities.ToList();
        }
    }
}