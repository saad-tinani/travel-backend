﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TravelAPI.Models;

namespace TravelAPI
{
    interface ITripDal : IDisposable
    {
        void AddTrip(Trip trip);
        Trip GetTrip(int id);
        List<Trip> GetAllTrips();
    }
}
