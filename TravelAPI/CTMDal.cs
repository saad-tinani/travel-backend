﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using Newtonsoft.Json.Linq;
using RestSharp;
using RestSharp.Extensions;
using SimpleJson;
using TravelAPI.Models;

namespace TravelAPI
{
    public class CTMDal
    {
        private static JObject maps = null;

        private string mapsStr =
                "{\"AGADIR\":\"002\",\"AGADIR TALBORJT\":\"832\",\"AGDEZ\":\"088\",\"AGOUIM\":\"463\",\"AHFIR\":\"260\",\"AIT MELLOUL\":\"200\",\"AIT WAFKA\":\"122\",\"AL HOCEIMA\":\"518\",\"ARGOUBE\":\"569\",\"ASSILAH\":\"031\",\"AZILAL\":\"099\",\"AZROU\":\"032\",\"BAB BERRED - MONEY EL KAHANE\":\"937\",\"BAB TAZA - MONEY EL KAHANE\":\"938\",\"BENI ENSAR\":\"244\",\"BENI MELLAL\":\"004\",\"BENI MELLAL - GARE ROUTIERE\":\"272\",\"BENI TADJIT\":\"009\",\"BERKANE\":\"035\",\"BOUARFA\":\"622\",\"BOUIZAKARNE\":\"865\",\"BOUJDOUR\":\"436\",\"BOUMALEN DADESS\":\"935\",\"CASA - AIN SEBAA\":\"745\",\"CASA - LAAOUINA\":\"537\",\"CASABLANCA\":\"001\",\"CHEFCHAOUE N\":\"071\",\"DAKHLA\":\"112\",\"EL JEBHA\":\"604\",\"ELJADIDA\":\"547\",\"ERFOUD\":\"041\",\"ERRACHIDIA\":\"010\",\"ESSAOUIRA\":\"079\",\"FES\":\"013\",\"FES BOUJLOUD\":\"271\",\"FIGUIG\":\"123\",\"FNIDEK\":\"181\",\"FOUM ZGUID\":\"232\",\"FQUIH BEN SALAH\":\"579\",\"GERGARAT\":\"568\",\"GOULMIMA\":\"043\",\"GOULMIME\":\"025\",\"GOURRAMA\":\"014\",\"GUERCIF\":\"044\",\"IFRANE\":\"536\",\"IMZOUREN\":\"140\",\"INZEGANE\":\"090\",\"JBEL AROUIT\":\"341\",\"K. SRAGHNA\":\"040\",\"K .TADLA\":\"059\",\"KACETA-AKACHAR\":\"933\",\"KELAA MAGOUNA\":\"092\",\"KHEMISSET\":\"047\",\"KHENIFRA\":\"632\",\"KHOURIBGA\":\"050\",\"LAAYOUNE\":\"072\",\"LAAYOUNE PORT\":\"292\",\"LAMHAMID GHOZLANE\":\"257\",\"LARACHE\":\"083\",\"MARRAKECH\":\"015\",\"MARRAKECH - GARE ROUTIERE\":\"273\",\"MEKNES\":\"016\",\"MIDAR\":\"038\",\"MIDELT\":\"052\",\"M'RIRT\":\"086\",\"NADOR\":\"017\",\"NADOR  GR\":\"275\",\"NKOB\":\"739\",\"OUARZAZATE\":\"018\",\"OUAZZANE\":\"053\",\"OUED LAABID- EL AYAC\":\"155\",\"OUEDZEM\":\"087\",\"OUJDA\":\"021\",\"OULAD BERREHIL\":\"924\",\"OULED TEIMA\":\"107\",\"RABAT\":\"022\",\"RICH C\":\"054\",\"RISSANI\":\"055\",\"SAFI\":\"023\",\"SAIDIA\":\"590\",\"SETTAT\":\"057\",\"SIDI IFNI\":\"598\",\"SIDI KACEM\":\"098\",\"SIDI MOKHTAR\":\"494\",\"SKOURA\":\"101\",\"SMARA\":\"186\",\"TAFRAOUT\":\"242\",\"TAGOUNIT\":\"060\",\"TALIOUINE\":\"210\",\"TALSINT\":\"061\",\"TAMANAR\":\"116\",\"TAMEGROUTE\":\"465\",\"TAN TAN\":\"102\",\"TANGER\":\"027\",\"TANGER GR\":\"445\",\"TAOUNATE\":\"509\",\"TARFAYA\":\"197\",\"TARGUIST\":\"096\",\"TAROUDANT\":\"388\",\"TATA\":\"380\",\"TAZA\":\"063\",\"TAZARINE\":\"520\",\"TAZNAKHTE\":\"211\",\"TENDRARA\":\"623\",\"TETOUAN\":\"028\",\"TISSINT\":\"567\",\"TIZNIT\":\"396\",\"ZAGORA\":\"464\",\"ZAIOU\":\"203\",\"ZAOUIT ECHEIKH- RAY A\":\"626\"}"
            ;
        public CTMDal()
        {
            maps = JObject.Parse(mapsStr);
        }
        //string[] split = date.Split("-".ToCharArray());
        public IRestResponse GetCTMOffers(string date, string depart, string arrivee)
        {
            var client = new RestClient("http://billetterie.ctm.ma/site/select_voyages_aller?datev=" + @date + "&agen_dep=" + @depart + "&agen_dest=" + @arrivee + "&nbp=1&aller=AS&tpay=MA&q=1&_search=false&nd=1513643188899&rows=30&page=1&sidx=&sord=");
            var request = new RestRequest(Method.GET);
            request.AddHeader("Host", "billetterie.ctm.ma");
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Access-Control-Allow-Origin", "*");
            request.AddHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
            IRestResponse response = client.Execute(request);
            return response;
        }

        public List<Voyage> GetCTMVoyage(DateTime date, string departure, string arrival)
        {
            List<Voyage> ret = new List<Voyage>();
            string departTime = date.ToString("dd-MM-yyyy");
            Console.WriteLine(departTime + ".");
            try
            {
                //departure = (string) maps[departure.ToUpper()];
                //arrival = (string) maps[arrival.ToUpper()];
                IRestResponse response = GetCTMOffers(departTime, departure, arrival);
                JObject resultJSon = JObject.Parse(response.Content);
                Console.WriteLine("Rows:" + resultJSon);
                JArray rows = (JArray)(resultJSon["rows"]);
                if (rows == null) return ret;
                Console.WriteLine("Rows:" + rows);
                foreach (var r in rows)
                {
                    Console.WriteLine(r);
                    JArray cells = (JArray)r["cell"];
                    Voyage voyage = new Voyage();
                    voyage.Arrival = new City();
                    voyage.Arrival.Name = arrival;
                    voyage.Depart = new City("0", departure);
                    voyage.Company = new Company();
                    voyage.Company.Name = "CTM";
                    for (var i = 2; i < cells.Count; i++)
                    {
                        string tmp = (string)cells[i];
                        if (i == 2)
                        {
                            voyage.DepartTime = tmp;
                            // voyage.DepartTime = DateTime.Parse(date.ToString("yyyy/MM/dd") + " " + tmp.Split(" ".ToCharArray())[0]);
                        }
                        else
                        if (i == 3)
                        {
                            //price
                            voyage.Price = Double.Parse(tmp, CultureInfo.InvariantCulture);
                        }
                        else if (i == 4)
                        {
                            if (tmp.Equals(""))
                            {
                                voyage.Type = "CONFORT";
                            }
                            else
                            {
                                voyage.Type = tmp;
                            }
                        }
                        else if (i == 5)
                        {
                            string[] els = tmp.Split(" ".ToCharArray());
                            if (els.Length >= 2)
                            {
                                if (els[els.Length - 1].Equals("Lendemain"))
                                {
                                    DateTime tmpD = new DateTime(date.Year, date.Month, date.Day);
                                    tmpD = tmpD.AddDays(1);
                                    voyage.ArrivalTime = tmp;
                                    // voyage.ArrivalTime = DateTime.Parse(tmpD.ToString("yyyy/MM/dd") + " " + els[0]);
                                }
                                else
                                {
                                    voyage.ArrivalTime = tmp;
                                    // voyage.ArrivalTime = DateTime.Parse(date.ToString("yyyy/MM/dd") + " " + els[0]);
                                }
                            }
                        }
                    }
                    ret.Add(voyage);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return ret;
            }
            return ret;
        }

        public String getCityName(int id)
        {
            String city;
            map.TryGetValue(id, out city);
            return city;
        }

        private static Dictionary<int, String> map = new Dictionary<int, string>
        {
            { 002, "AGADIR" },
            { 832, "AGADIR TALBORJT" },
            { 088, "AGDEZ" },
            { 463, "AGOUIM" },
            { 260, "AHFIR" },
            { 037, "AHFIR-HABRI" },
            { 200, "AIT MELLOUL" },
            { 518, "AL HOCEIMA" },
            { 031, "ASSILAH" },
            { 099, "AZILAL" },
            { 032, "AZROU" },
            { 937, "BAB BERRED - MONEY EL KAHANE" },
            { 938, "BAB TAZA - MONEY EL KAHANE" },
            { 244, "BENI ENSAR" },
            { 004, "BENI MELLAL" },
            { 272, "BENI MELLAL - GARE ROUTIERE" },
            { 009, "BENI TADJIT" },
            { 035, "BERKANE" },
            { 572, "BIRGUENDOUZ" },
            { 622, "BOUARFA" },
            { 865, "BOUIZAKARNE" },
            { 436, "BOUJDOUR" },
            { 745, "CASA - AIN SEBAA" },
            { 537, "CASA - LAAOUINA" },
            { 001, "CASABLANCA" },
            { 071, "CHEFCHAOUEN" },
            { 112, "DAKHLA" },
            { 604, "EL JEBHA" },
            { 547, "ELJADIDA" },
            { 041, "ERFOUD" },
            { 010, "ERRACHIDIA" },
            { 079, "ESSAOUIRA" },
            { 013, "FES" },
            { 271, "FES BOUJLOUD" },
            { 123, "FIGUIG" },
            { 181, "FNIDEK" },
            { 232, "FOUM ZGUID" },
            { 579, "FQUIH BEN SALAH" },
            { 568, "GERGARAT" },
            { 043, "GOULMIMA" },
            { 025, "GOULMIME" },
            { 014, "GOURRAMA" },
            { 044, "GUERCIF" },
            { 536, "IFRANE" },
            { 140, "IMZOUREN" },
            { 090, "INZEGANE" },
            { 341, "JBEL AROUIT" },
            { 975, "JERRADA" },
            { 040, "K. SRAGHNA" },
            { 933, "KACETA-AKACHAR" },
            { 957, "KASBAT TADLA" },
            { 092, "KELAA MAGOUNA" },
            { 574, "KENITRA" },
            { 047, "KHEMISSET" },
            { 632, "KHENIFRA" },
            { 050, "KHOURIBGA" },
            { 072, "LAAYOUNE" },
            { 292, "LAAYOUNE PORT" },
            { 257, "LAMHAMID GHOZLANE" },
            { 083, "LARACHE" },
            { 015, "MARRAKECH" },
            { 273, "MARRAKECH - GARE ROUTIERE" },
            { 016, "MEKNES" },
            { 538, "MIDAR" },
            { 052, "MIDELT" },
            { 086, "M'RIRT" },
            { 017, "NADOR" },
            { 275, "NADOR GR" },
            { 739, "NKOB" },
            { 141, "OUALIDIA." },
            { 018, "OUARZAZATE" },
            { 053, "OUAZZANE" },
            { 155, "OUED LAABID" },
            { 087, "OUEDZEM" },
            { 021, "OUJDA" },
            { 924, "OULAD BERREHIL" },
            { 107, "OULED TEIMA" },
            { 022, "RABAT" },
            { 054, "RICH C" },
            { 055, "RISSANI" },
            { 023, "SAFI" },
            { 590, "SAIDIA" },
            { 057, "SETTAT" },
            { 598, "SIDI IFNI" },
            { 098, "SIDI KACEM" },
            { 494, "SIDI MOKHTAR" },
            { 101, "SKOURA" },
            { 186, "SMARA" },
            { 242, "TAFRAOUT" },
            { 060, "TAGOUNIT" },
            { 210, "TALIOUINE" },
            { 634, "TALSINT CT" },
            { 116, "TAMANAR" },
            { 465, "TAMEGROUTE" },
            { 102, "TAN TAN" },
            { 027, "TANGER" },
            { 445, "TANGER GR" },
            { 158, "TANGER-PANORAMA CACH" },
            { 509, "TAOUNATE" },
            { 197, "TARFAYA" },
            { 096, "TARGUIST" },
            { 388, "TAROUDANT" },
            { 380, "TATA" },
            { 063, "TAZA" },
            { 520, "TAZARINE" },
            { 211, "TAZNAKHTE" },
            { 623, "TENDRARA" },
            { 028, "TETOUAN" },
            { 065, "TINEGHIR" },
            { 066, "TINJDAD" },
            { 132, "TINZOULINE" },
            { 396, "TIZNIT" },
            { 464, "ZAGORA" },
            { 203, "ZAIOU" },
            { 626, "ZAOUIT ECHEIKH" },
        };

    }

}