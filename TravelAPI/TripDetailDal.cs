﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TravelAPI.Models;

namespace TravelAPI
{
    public class TripDetailDal : ITripDetailDal
    {
        private BddContext bdd;

        public TripDetailDal()
        {
            bdd = new BddContext();
        }
        public void Dispose()
        {
            bdd.Dispose();
        }

        public void AddTripDetail(TripDetail tripDetail)
        {
            bdd.TripDetails.Add(tripDetail);
            bdd.SaveChanges();
        }

        public TripDetail GetTripDetail(int id)
        {
            TripDetail tripDetail = bdd.TripDetails.Find(id);
            if (tripDetail != null)
            {
                tripDetail.Trip = bdd.Trips.Find(tripDetail.TripId);
                tripDetail.DepartureLocation = bdd.Cities.Find(tripDetail.DepartureLocationId);
                tripDetail.ArrivalLocation = bdd.Cities.Find(tripDetail.ArrivalLocationId);
                return tripDetail;
            } else
            {
                return null;
            }
        }

        public List<TripDetail> GetAllTripsDetails()
        {
            List<TripDetail> tripDetails = bdd.TripDetails.ToList();
            foreach (var tripDetail in tripDetails)
            {
                tripDetail.Trip = bdd.Trips.Find(tripDetail.TripId);
                tripDetail.DepartureLocation = bdd.Cities.Find(tripDetail.DepartureLocationId);
                tripDetail.ArrivalLocation = bdd.Cities.Find(tripDetail.ArrivalLocationId);
            }
            return tripDetails;
        }
    }
}