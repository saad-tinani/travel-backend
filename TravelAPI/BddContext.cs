﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using TravelAPI.Models;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace TravelAPI
{
    public class BddContext : DbContext
    {
        public BddContext() : base("TravelAPI")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<BddContext, TravelAPI.Migrations.Configuration>());
        }

        public DbSet<City> Cities { get; set; }
        public DbSet<Trip> Trips { get; set; }
        public DbSet<TripDetail> TripDetails { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }
    }
}