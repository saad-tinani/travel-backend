﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using TravelAPI.Models;

namespace TravelAPI
{
    public class TripDal : ITripDal
    {
        private BddContext bdd;

        public TripDal()
        {
            bdd = new BddContext();
        }
        public void Dispose()
        {
            bdd.Dispose();
        }

        public void AddTrip(Trip trip)
        {
            bdd.Trips.Add(trip);
            bdd.SaveChanges();
        }

        public Trip GetTrip(int id)
        {
            Trip trip = bdd.Trips.Find(id);
            if (trip != null)
            {
                trip.DepartureLocation = bdd.Cities.Find(trip.DepartureLocationId);
                trip.ArrivalLocation = bdd.Cities.Find(trip.ArrivalLocationId);
                return trip;
            } else
            {
                return null;
            }
        }

        public List<Trip> GetAllTrips()
        {
            List<Trip> trips = bdd.Trips.ToList();
            foreach (var trip in trips)
            {
                trip.DepartureLocation = bdd.Cities.Find(trip.DepartureLocationId);
                trip.ArrivalLocation = bdd.Cities.Find(trip.ArrivalLocationId);
            }
            return trips;
        }
    }
}