﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Threading.Tasks;
using TravelAPI.Models;
using System.Web.Http.Cors;

namespace TravelAPI.Controllers
{
    [EnableCors(origins: "http://localhost:4200", headers: "*", methods: "*")]
    public class TripController : ApiController
    {
        [HttpPost]
        public Trip Add([FromBody]Trip trip)
        {
            using (ITripDal dal = new TripDal())
            {
                dal.AddTrip(trip);
                return dal.GetTrip(trip.Id);
            }
        }

        [HttpGet]
        public Trip Get(int id)
        {
            using (ITripDal dal = new TripDal())
            {
                return dal.GetTrip(id);
            }
        }

        [HttpGet]
        public List<Trip> Get()
        {
            using (ITripDal dal = new TripDal())
            {
                return dal.GetAllTrips();
            }
        }
    }
}
