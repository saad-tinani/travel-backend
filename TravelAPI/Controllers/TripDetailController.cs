﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TravelAPI.Models;
using System.Web.Http.Cors;

namespace TravelAPI.Controllers
{
    [EnableCors(origins: "http://localhost:4200", headers: "*", methods: "*")]
    public class TripDetailController : ApiController
    {
        [HttpPost]
        public TripDetail Add([FromBody]TripDetail tripDetail)
        {
            using (ITripDetailDal dal = new TripDetailDal())
            {
                dal.AddTripDetail(tripDetail);
                return dal.GetTripDetail(tripDetail.Id);
            }
        }

        [HttpGet]
        public TripDetail Get(int id)
        {
            using (ITripDetailDal dal = new TripDetailDal())
            {
                return dal.GetTripDetail(id);
            }
        }

        [HttpGet]
        public List<TripDetail> Get()
        {
            using (ITripDetailDal dal = new TripDetailDal())
            {
                return dal.GetAllTripsDetails();
            }
        }
    }
}
