﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using TravelAPI.Models;
using System.Web.Http.Cors;

namespace TravelAPI.Controllers
{
    [EnableCors(origins: "http://localhost:4200", headers: "*", methods: "*")]
    public class CityController : ApiController
    {
        [HttpPost]
        public City Add([FromBody]City city)
        {
            using (ICityDal dal = new CityDal())
            {
                dal.AddCity(city);
                return dal.GetCity(city.Id);
            }
        }

        [HttpGet]
        public City Get(int id)
        {
            using (ICityDal dal = new CityDal())
            {
                return dal.GetCity(id);
            }
        }

        [HttpGet]
        public List<City> Get()
        {
            using (ICityDal dal = new CityDal())
            {
                return dal.GetAllCities();
            }
        }
    }
}